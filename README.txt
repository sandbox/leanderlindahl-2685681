CONTENTS OF THIS FILE
---------------------
   
 * Installation
 * Configuration


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

Once you've activated the module a new block will be available in Structure -> Block Layout. Choose the region in which you'd like to place the Facebook Page Plugin and press the button 'Place block'. In the modal dialogue choose Facebook Page Box. You will be presented with the 'Configure block' screen where you can provide a title (optional) for the block and also the URL of the Facebook Page you want to display in your block. Type the URL in the field 'Facebook Page URL' and hit 'Save block'. The block will now appear the block region.