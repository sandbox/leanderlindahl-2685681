<?php 
/**
 * Provides a 'Facebook Page Box' Block
 *
 * @Block(
 *   id = "facebook_page_box_block",
 *   admin_label = @Translation("Facebook Page Box"),
 * )
 */
namespace Drupal\facebook_page_box\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

class facebook_page_box_block extends BlockBase implements BlockPluginInterface {
  
  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['url'])) {
      $url = $config['url'];
    }
    else {
      $url = $this->t('not set');
    }

  	return array(
      '#theme' => 'block--fbbox',
      '#description' => 'description from block.php',
      '#url' => $url,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['facebook_page_box_block_url'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Facebook Page URL'),
      '#description' => $this->t('Enter the URL to your Facebook Page, i. e. <i>https://www.facebook.com/Drupal-8427738891</i>'),
      '#default_value' => isset($config['url']) ? $config['url'] : ''
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('url', $form_state->getValue('facebook_page_box_block_url'));
  }

}
?>